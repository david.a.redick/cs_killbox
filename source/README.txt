Version:
	Beta

About:
	cs_killbox is a very small map.  Excellent for warm up or practice.

	Max players is 32.  Not really recommended since it gets too crowded.

	12 or 16 players is a sweet spot for serious playing.

Contents:
	cs_killbox.prt
	cs_killbox.vmf
	cs_killbox.vmx

Instructions:

	Start up the Source SDK and the Hammer Editor.
	Load cs_killbox.vmf and edit.

	Edit the image "cs_killbox.pdn" using Paint.Net
	http://www.getpaint.net

Author:
	David A. Redick
	https://github.com/david-redick/cs_killbox
	tinyweldingtorch@gmail.com

Legal:
	Released under the GNU General Public License version 2.
