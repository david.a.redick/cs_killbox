cs_killbox is a small, but well balanced, close quarters battle (CQB) map.

It is excellent for warm up or practice and has a twist on the standard hostage maps.

Instead of the hostages being in the Terrorist spawn, they are positioned mid-way in the map.
Sounds insane but it works quite nicely.

Max players is 32. 
Not really recommended since it gets too crowded.

12 or 16 players is a sweet spot for serious playing.
