Version:
	Beta

About:
	cs_killbox is a very small map.  Excellent for warm up or practice.

	Max players is 32.  Not really recommended since it gets too crowded.

	12 or 16 players is a sweet spot for serious playing.

Contents:
	cs_killbox.bsp
	cs_killbox.nav

Instructions:
	Place both files into:
	X:\Steam\SteamApps\UserName\counter-strike source\cstrike\maps\

	Where "X" is your drive letter and "UserName" is your steam account user name.

Author:
	David A. Redick
	http://tinyweldingtorch.org
	davidredick@bellsouth.net

Legal:
	Released under the GNU General Public License version 2.